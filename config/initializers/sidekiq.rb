# frozen_string_literal: true

url = "#{ENV.fetch('REDIS_URL')}:#{ENV.fetch('REDIS_PORT')}"
use_ssl = ENV.fetch('REDIS_USE_SSL').in?(['true', true])
password = ENV.fetch('REDIS_AUTH_KEY', nil)

Sidekiq.configure_server do |config|
  config.redis = { id: nil,
                   url: url,
                   size: Integer(ENV.fetch('SIDEKIQ_CONCURRENCY', 5)) + 5,
                   password: password,
                   ssl: use_ssl }
end

Sidekiq.configure_client do |config|
  config.redis = { id: nil,
                   url: url,
                   size: Integer(ENV.fetch('SIDEKIQ_CONCURRENCY', 5)) + 5,
                   password: password,
                   ssl: use_ssl }
end
