class CreateResults < ActiveRecord::Migration[6.0]
  def change
    create_table :results do |t|
      t.jsonb :downloaded_data, default: {}
      t.jsonb :input, default: {}
      t.references :email_template, foreign_key: true, index: true
      t.references :user, foreign_key: true, index: true
      t.integer :status, default: 0
      t.datetime :sent_at
      t.timestamps
    end
  end
end
