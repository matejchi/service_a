class CreateEmailTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :email_templates do |t|
      t.string :template_name, null: false
      t.string :email_type, default: 'twitter'
      t.string :sender_name
      t.string :subject
      t.text :body
      t.string :description
      t.references :user, foreign_key: true, index: true
      t.timestamps
    end
  end
end
