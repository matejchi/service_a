# frozen_string_literal: true

user = User.first_or_create!(email: 'brucelee@isp.net', name: 'Bruce')
EmailTemplate.first_or_create!(template_name: 'Twitter',
                               user_id: user.id,
                               sender_name: user.name,
                               body: "<h2>Hi {{user_name}},</h2>\r\n<p>Here are your tweeter URLs:</p>\r\n<p style=\"text-align: center;\">{{tweets}}</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>")

puts 'Seed finished.'
