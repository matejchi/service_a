# frozen_string_literal: true

require 'test_helper'

class NotificationWorkerTest < ActiveSupport::TestCase
  setup do
    stub_request(:post, 'http://localhost:3005/notifications')
    .with( body: {'result_id'=>'1'},
           headers: { 'Accept'=>'*/*',
                      'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                      'Content-Type'=>'application/x-www-form-urlencoded',
                      'User-Agent'=>'Faraday v1.0.1',
                      'X-Service-A-Identifier'=>'b2d8f9b8b3853f91572979decf67194b38d2cab08c1a03a96cb21d0ce3ae1dc5ace3be104d6cbdbeaba9098f1a37e15248585b04b1426542b57c8a78f391b857'
                      }).to_return(status: 200, body: '', headers: {})

    stub_request(:post, 'http://localhost:3005/notifications')
    .with(body: {'result_id'=>'22'},
          headers: {
            'Accept'=>'*/*',
            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type'=>'application/x-www-form-urlencoded',
            'User-Agent'=>'Faraday v1.0.1',
            'X-Service-A-Identifier'=>'b2d8f9b8b3853f91572979decf67194b38d2cab08c1a03a96cb21d0ce3ae1dc5ace3be104d6cbdbeaba9098f1a37e15248585b04b1426542b57c8a78f391b857'
    }).to_return(status: 404, body: '', headers: {})

  end

  test 'will update result record status' do
    result = results(:result_record)

    assert_equal(result.status, 'created')

    Sidekiq::Testing.inline! do
      NotificationWorker.perform_async(result.id)
    end

    assert_equal result.reload.status, 'sent'
  end

  test 'will not update result record status if result id is wrong, instead it will raise error' do
    Sidekiq::Testing.inline! do
      assert_raises RuntimeError do
        NotificationWorker.perform_async(22)
      end
    end
  end
end
