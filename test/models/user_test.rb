# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  context 'associations' do
    should have_many(:email_templates).class_name('EmailTemplate')
    should have_many(:results).class_name('Result')
  end

  context 'validations' do
    should validate_presence_of(:email)
    should validate_uniqueness_of(:email)
    should validate_presence_of(:name)
    should_not allow_value('userATisp.net').for(:email)
    should_not allow_value('user@isp-net').for(:email)
    should allow_value('user@isp.net').for(:email)
  end
end
