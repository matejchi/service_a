# frozen_string_literal: true

require 'application_system_test_case'

class EmailTemplatesTest < ApplicationSystemTestCase
  setup do
    @email_template = email_templates(:one)
  end

  test 'visiting the index' do
    visit email_templates_url
    assert_selector "h3", text: 'Email templates'
  end

  test 'creating a Email template without template name will trigger client side validation' do
    visit email_templates_url
    click_on 'New Email Template'

    fill_in 'sender_name', with: 'matej'
    fill_in 'description', with: 'desc'
    fill_in 'subject', with: 'email subject'

    click_on 'Create Email template'

    message = page.find('#template_name').native.attribute('validationMessage')
    assert_equal message, 'Please fill out this field.'
    assert_equal current_path, new_email_template_path
  end

  test 'updating a Email template without template name will trigger client side validation' do
    visit email_templates_url
    click_on "Edit", match: :first

    fill_in 'template_name', with: ''

    click_on "Update Email template"

    message = page.find('#template_name').native.attribute('validationMessage')
    assert_equal message, 'Please fill out this field.'
  end

  test 'destroying a Email template' do
    visit email_templates_url

    templates_count = page.all('tbody tr').count

    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    templates_count2 = page.all('tbody tr').count

    assert_equal templates_count, 2
    assert_equal templates_count2, 1
  end
end
