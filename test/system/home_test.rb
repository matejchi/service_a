# frozen_string_literal: true

require 'application_system_test_case'

class HomeTest < ApplicationSystemTestCase
  test 'visiting the index' do
    visit root_url
    assert_selector('h3', text: 'Fetch tweets')
  end

  test 'will display message in alert box if request is successful' do
    visit root_url

    fill_in('receiver_email', with: 'user@isp.net')

    msg = accept_alert do
      click_on('Fetch')
    end

    assert_equal(msg, 'Fetching of results has been initiated...')
  end

  test "will trigger client side validation for 'receiver email' if not given" do
    visit root_url

    click_on('Fetch')

    msg = page.find('#receiver_email').native.attribute('validationMessage')
    assert_equal(msg, 'Please fill out this field.')
  end

  test "will trigger client side validation for 'receiver email' if email format is wrong" do
    visit root_url

    fill_in('receiver_email', with: 'userATisp.net')
    click_on('Fetch')

    msg = page.find('#receiver_email').native.attribute('validationMessage')
    assert_equal(msg, "Please include an '@' in the email address. 'userATisp.net' is missing an '@'.")
  end
end
