# frozen_string_literal: true

require 'test_helper'
require 'twitter_client_mock'

class ResultsServiceTest < ActiveSupport::TestCase
  setup do
    period_from = 2.days.ago.to_s(:iso8601).slice(0,16)
    @user = users(:first_user)
    @params = { input: { email_template_id: 1, receiver_email: 'user@isp.net', period_from: period_from, period_to: '' } }
  end

  context '#call' do
    should 'create new result record, and update it with downloaded_data' do
      assert_difference('Result.count') do
        ResultsService.new(@user, @params, TwitterClientMock.client).call
      end

      assert_not_nil Result.last.downloaded_data
      refute_empty Result.last.downloaded_data
    end

    should 'notify service B' do
      service = ResultsService.new(@user, @params, TwitterClientMock.client)
      service.expects(:notify_service_b).once
      service.call
    end
  end
end
