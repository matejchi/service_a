# frozen_string_literal: true

require 'test_helper'

class TwitterServiceTest < ActiveSupport::TestCase
  context '#client' do
    should 'return TwitterAPI client object' do
      client = TwitterService.client

      assert_instance_of(Twitter::REST::Client, client)
      assert_respond_to(client, :home_timeline)
    end
  end

  # you can check Twitter ID and timestamp here: https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-home_timeline#example-response
  context '#time_to_tweet_id' do
    should 'convert DateTime object to Twitter ID' do
      date = "2018-10-10T20:19"
      date = DateTime.parse(date)
      result = TwitterService.time_to_tweet_id(date)

      # testing for first 7 chars...because #time_to_tweet_id method gives as precision in minutes, not seconds or miliseconds..
      assert_match(/1050118/, result.to_s)
    end
  end

  context '#tweet_id_to_time' do
    should 'convert Twitter ID to DateTime UTC' do
      id = 1050118621198921728

      result = TwitterService.tweet_id_to_time(id)
      assert_equal result, '2018-10-10 20:19:24 UTC'
    end
  end
end
