# frozen_string_literal: true

require 'test_helper'

class EmailTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:first_user)
    @email_template = email_templates(:one)
  end

  context 'INDEX' do
    should 'get index' do
      get email_templates_url
      assert_response :success
    end
  end

  context 'NEW' do
    should 'get new' do
      get new_email_template_url
      assert_response :success
    end
  end

  context 'CREATE' do
    should 'create email_template' do
      assert_difference('EmailTemplate.count') do
        post email_templates_url, params: { email_template: { template_name: 'new template'} }
      end

      assert_equal EmailTemplate.last.template_name, 'new template'
      assert_redirected_to email_templates_url
    end

    should 'not create email_template if template name is missing' do
      assert_no_difference('EmailTemplate.count') do
        post email_templates_url, params: { email_template: { template_name: ''} }
      end
    end
  end

  context 'SHOW' do
    should 'show email_template' do
      get email_template_url(@email_template)
      assert_response :success
    end

    should 'raise error if email template id is wrong' do
      assert_raises ActiveRecord::RecordNotFound do
        get email_template_url(22)
      end
    end
  end

  context 'EDIT' do
    should 'get edit' do
      get edit_email_template_url(@email_template)
      assert_response :success
    end

    should 'raise error if email template id is wrong' do
      assert_raises ActiveRecord::RecordNotFound do
        get edit_email_template_url(22)
      end
    end
  end

  context 'UPDATE' do
    should 'update email_template' do
      patch email_template_url(@email_template), params: { email_template: { template_name: 'changed name' } }
      assert_equal  @email_template.reload.template_name, 'changed name'
      assert_redirected_to email_templates_url
    end

    should 'raise error if email_template id is wrong' do
      assert_raises ActiveRecord::RecordNotFound do
        patch email_template_url(22), params: { email_template: { template_name: 'changed name' } }
      end
    end
  end

  context 'DELETE' do
    should 'destroy email_template' do
      assert_difference('EmailTemplate.count', -1) do
        delete email_template_url(@email_template)
      end

      assert_redirected_to email_templates_url
    end

    should 'not destroy email_template if id is wrong, error is raised, instead' do
      email_templates_count = EmailTemplate.count

      assert_raises ActiveRecord::RecordNotFound do
        delete email_template_url(22)
      end

      assert_equal email_templates_count, EmailTemplate.count
    end
  end
end
