# frozen_string_literal: true

require 'test_helper'

class ResultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @params = { input: { email_template_id: 1, receiver_email: 'user@isp.net', period_from: '', period_to: '' } }
    stub_request(:get, /.*api.twitter.com.*/).with(headers: {'User-Agent' => 'TwitterRubyGem/7.0.0'}).to_return(status: 200, body: "", headers: {})
  end

  context 'FETCH' do
    should 'initiate fetching of tweets' do
      post fetch_results_url, xhr: true, params: @params

      assert_response :success
      assert_match(/Fetching of results has been initiated.../, response.parsed_body)
    end

    should 'create new result record' do
      assert_difference('Result.count') do
        post fetch_results_url, xhr: true, params: @params
      end

      assert_response :success
    end

    should 'display error if params are wrong' do
      post fetch_results_url, xhr: true, params: @params.deep_merge(input: { receiver_email: ''})

      assert_match(/Something went wrong: Receiver Email param is missing/, response.parsed_body)
    end
  end
end
