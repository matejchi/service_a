# frozen_string_literal: true

module TwitterClientMock
  def self.client
    self
  end

  def self.home_timeline(params)
    file = File.read('test/fixtures/files/twitter_response.json')
    body = JSON.parse(file)
    body
  end
end
