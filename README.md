# Service A
Service A should be run by using 2 separate processes:
        - Rails app server
        - Rails background processing - Sidekiq process

**Dependencies:**
  - Ruby v 2.7.1
  - Rails 6.0.3
  - PostgreSQL > 9.3
  - Redis 4.0.9 (probably other versions will work, but this one is used when developing)
  - .env file is included in commit. It's an exception of the rule, since this is an exercise.

**To setup Service A do:**
  1) Rails app server instance:
   ```sh
   bundle install
   bundle exec rails db:create
   bundle exec rails db:migrate
   bundle exec rails db:seed
   bundle exec rails s
   open browser and navigate to: http://localhost:3000
  ```
  2) Sidekiq instance
  ```sh
  dotenv bundle exec sidekiq
  ```

  - note: 'dotenv-rails' gem is used for EN
