# frozen_string_literal: true

class NotificationWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  SERVICE_B_URL = ENV.fetch('SERVICE_B_URL', 'http://localhost:3005/notifications')
  HEADER_SECRET = ENV.fetch('SERVICE_A_HEADER_SECRET', '')

  def perform(result_record_id)
    notify_service_b(result_record_id)
  rescue StandardError => e
    Sidekiq.logger.error(e.message)
    raise e
  end

  private

  def notify_service_b(result_record_id)
    req_headers = { 'X-SERVICE-A-IDENTIFIER': HEADER_SECRET }
    res = Faraday.post(SERVICE_B_URL, { result_id: result_record_id }, req_headers)

    if res.status == 200
      Result.find(result_record_id).update!(sent_at: Time.current, status: Result::statuses[:sent])
    else
      msg = JSON.parse(res.body).dig('error') rescue nil
      raise "Failed when calling service B: message: #{msg}"
    end
  end
end
