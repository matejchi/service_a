# frozen_string_literal: true

class ResultsService
  def initialize(user, params, twitter_client)
    @user = user
    @params = params
    @twitter_client = twitter_client
  end

  def call
    create_result_record
    fetch_tweets
    notify_service_b
  rescue StandardError => _e
    @result_record.update!(status: Result::statuses[:failed]) if @result_record
  end

  private

  def create_result_record
    @result_record = Result.create(email_template_id: @params.dig(:input, :email_template_id),
                                   user_id: @user.id,
                                   input: @params[:input])
  end

  def fetch_tweets
    return unless @result_record.email_template.email_type == 'twitter'

    twitter_params = prepare_twitter_params
    data = @twitter_client.home_timeline(twitter_params)
    @result_record.update!(downloaded_data: data, status: Result::statuses[:downloaded])
  end

  def prepare_twitter_params
    # current twitter API does not support querying by time(stamp), instead it uses 'since_id', 'max_id' params which are tweet ids
    # we'll need to convert dates to timestamps -> twitter ids

    twitter_params = { count: ENV.fetch('TWEETS_COUNT', 20),
                       exclude_replies: true,
                       include_entities: true,
                       trim_user: true }

    since_id = @params.dig(:input, :period_from)
    max_id = @params.dig(:input, :period_to)

    since_id = 2.days.ago.to_s(:iso8601) unless since_id.present? && max_id.present? # fallback to since 2 days ago if nothing is selected
    since_id = TwitterService.time_to_tweet_id(DateTime.parse(since_id))
    twitter_params[:since_id] = since_id

    if max_id.present?
      max_id = TwitterService.time_to_tweet_id(DateTime.parse(max_id))
      twitter_params[:max_id] = max_id
    end

    twitter_params
  end

  def notify_service_b
    NotificationWorker.perform_async(@result_record.id)
  end
end
