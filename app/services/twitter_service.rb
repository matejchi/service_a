# frozen_string_literal: true

require 'twitter'

module TwitterService
  TWITTER_EPOCH_OFFSET = 1288834974657

  def self.client
    @twitter_client ||= Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV.fetch('TWITTER_API_KEY', '')
      config.consumer_secret     = ENV.fetch('TWITTER_API_SECRET_KEY', '')
      config.access_token        = ENV.fetch('TWITTER_ACCESS_TOKEN', '')
      config.access_token_secret = ENV.fetch('TWITTER_ACCESS_TOKEN_SECRET', '')
    end
  end

  def self.time_to_tweet_id(time)
    timestamp = ((time.utc.to_i * 1000) - TWITTER_EPOCH_OFFSET)
    timestamp << 22
  end

  def self.tweet_id_to_time(tweet_id)
    Time.at(((tweet_id >> 22) + TWITTER_EPOCH_OFFSET)/1000).utc
  end
end
