# frozen_string_literal: true

class EmailTemplate < ApplicationRecord
  EMAIL_TYPES = %w[twitter linkedin]

  has_many :results, dependent: :nullify
  belongs_to :user

  validates :template_name, presence: true, uniqueness: true
  validates :user, presence: true
  validates :email_type, inclusion: { in: EMAIL_TYPES, message: 'Email type not supported' }
end
