# frozen_string_literal: true

class ResultsController < ApplicationController
  def fetch
    check_params
    ResultsService.new(current_user, params, TwitterService.client).call
  rescue StandardError => e
    @error = e
  ensure
    respond_to { |format| format.js }
  end

  private

  def check_params
    raise 'Receiver Email param is missing' unless params.dig(:input, :receiver_email).present?
  end
end
