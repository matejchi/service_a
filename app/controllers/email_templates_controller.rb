# frozen_string_literal: true

class EmailTemplatesController < ApplicationController
  before_action :find_email_template, only: [:show, :edit, :update, :destroy]

  def index
    @email_templates = EmailTemplate.all
  end

  def show
  end

  def new
    @email_template = EmailTemplate.new
  end

  def edit
  end

  def create
    @email_template = EmailTemplate.new(email_template_params.merge(user_id: @current_user.id))

    if @email_template.save
      redirect_to email_templates_path, notice: 'Email template was successfully created.'
    else
      render :new
    end
  end

  def update
    if @email_template.update(email_template_params)
      redirect_to email_templates_path, notice: 'Email template was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @email_template.destroy!
    redirect_to email_templates_path, notice: 'Email template was successfully deleted.'
  end

  private

  def find_email_template
    @email_template = EmailTemplate.find(params[:id])
  end

  def email_template_params
    params.fetch(:email_template, {}).permit(:template_name, :sender_name, :subject, :description, :body)
  end
end
